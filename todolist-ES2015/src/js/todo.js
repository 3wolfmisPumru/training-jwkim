import CONST from './constant.js';
import TodoItem from './todoitem.js';
import DomHandler from './domhandler.js';

export default class Todo {
    /**
     * initialize
     * @param {Object} domElements - event target elements
     */
    constructor(domElements) {
        this.todoHashMap = new Map();
        this.todoList = [];
        this.lastSeq = 0;
        this.filterType = CONST.FILTER_STATE.ALL;
        this.domHandler = new DomHandler(domElements);
        this._addEvent();
    }

    /**
     * add todo
     * @param {string} todoname - todo name
     */
    addTodo(todoname) {
        if (!todoname) {
            throw new Error('needTodoName');
        }
        this.lastSeq += 1;
        this.todoHashMap.set(this.lastSeq, new TodoItem(todoname, this.lastSeq));
        this.draw();
    }
    /**
     * delete uncompleted list
     */
    removeUncompleteTodo() {
        this.todoHashMap.forEach((obj, key) => {
            if (obj.complete) {
                this.todoHashMap.delete(key);
            }
        });
    }
    /**
     * todolist filterling
     * @returns {Array.<TodoItem>} - todolist array
     */
    updateArray() {
        this._makeTodoListArray();
        switch (this.filterType) {
            case CONST.FILTER_STATE.COMPLETE:
                this.todoList = this.completeArray;
                break;
            case CONST.FILTER_STATE.UNCOMPLETE:
                this.todoList = this.unCompleteArray;
                break;
            case CONST.FILTER_STATE.ALL:
            default:
                this.todoList = this.unCompleteArray.concat(this.completeArray);
                break;
        }

        return this.todoList;
    }
    /**
     * draw list at HTMLElement
     */
    draw() {
        this.updateArray();
        this.domHandler.printTodoListElement(this.todoList);
        this.domHandler.printInfoCountElement(this.unCompleteArray.length, this.completeArray.length);
        this.domHandler.printFilterElement(this.filterType);
    }
    /**
     * make todo array list
     */
    _makeTodoListArray() {
        let sortingCreatedDesc = function(prev, next) {
            return next.createDt - prev.createDt;
        };

        this.completeArray = Array.from(this.todoHashMap.values()).filter(function(obj) {
            return obj.complete;
        });
        this.unCompleteArray = Array.from(this.todoHashMap.values()).filter(function(obj) {
            return !obj.complete;
        });
        this.completeArray.sort(sortingCreatedDesc);
        this.unCompleteArray.sort(sortingCreatedDesc);
    }

    /**
     * addEventHandler
     */
    _addEvent() {
        this.domHandler.onInputKeyDownHandler((todoname) => {
            this.addTodo(todoname);
        });
        this.domHandler.onTodoCheckBoxClickHandler((seq) => {
            this.todoHashMap.get(seq).toggleComplete();
            this.draw();
        });
        this.domHandler.onRemoveButtonClickHandler(() => {
            this.removeUncompleteTodo();
            this.draw();
        });
        this.domHandler.onFilterChangeButtonClickHandler((newFilterType) => {
            if (this.filterType !== newFilterType) {
                this.filterType = newFilterType;
                this.draw();
            }
        });
    }
}

