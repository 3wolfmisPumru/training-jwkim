
var tui = require('tui-code-snippet');
var util = require('./util');
var ToolTipItem = require('./tooltipitem');

var tooltip = {

    elementInstances: {},

    /**
     * @param {string} selectorName - tooltip target selector name
     * @param {Object} userOption - tooltip user option
     * @returns {boolean}
     */
    add: function(selectorName, userOption) {
        var option = setDefalutOption(userOption);

        if (this._checkOverLapSelectorChk(selectorName)) {
            return false;
        }

        this.elementInstances[selectorName] = new ToolTipItem({
            selectorName: selectorName,
            contents: option.contents,
            delay: option.delay,
            targetElements: document.querySelectorAll(selectorName),
            onMouseOverHandler: this._onMouseOverEventHandler.bind(this, selectorName),
            onMouseLeaveHandler: this._onMouseLeaveEventHandler.bind(this, selectorName)
        });
        this.elementInstances[selectorName].addToolTipEvent();

        return true;
    },
    /**
     * tooltip remove
     * @param {string} selectorName - tooltip target selector name
     */
    remove: function(selectorName) {
        var tooltipInstance = this.elementInstances[selectorName];
        if (tooltipInstance) {
            tooltipInstance.removeTooltipEvent();
            delete this.elementInstances[selectorName];
        }
    },
    /**
     * tooltip edit
     * @param {string} selectorName - tooltip target selector name
     * @param {Object} userOption - tooltip user option
     */
    edit: function(selectorName, userOption) {
        this.remove(selectorName);
        this.add(selectorName, userOption);
    },
    /**
     * tooltip init
     * @param {Array} toolTipOptionArray - tooltip init optionsArray
     */
    init: function(toolTipOptionArray) {
        removeAllInstance(this.elementInstances);
        this.elementInstances = [];
        toolTipOptionArray.forEach(function(userOption) {
            this.add(userOption.element, setDefalutOption(userOption));
        }.bind(this));
    },

    /**
    * tooltip mouseoverevent
    * @param {string} selectorName - tooltip target selector name
    * @param {Object} event - mouseleave event object
    */
    _onMouseOverEventHandler: function(selectorName, event) {
        var eventTarget = null;
        var tooltipInstance = this.elementInstances[selectorName];

        eventTarget = util.findEventTarget(event);

        if (tooltipInstance.timeout === null) {
            eventTarget = tooltipInstance.addToolTipElement(eventTarget);
            displayToolTip(tooltipInstance, eventTarget);
        }
    },

    /**
    * tooltip mouseLeaveEvent
    * @param {string} selectorName - tooltip target selector name
    * @param {EventObject} event - mouseleave event object
    */
    _onMouseLeaveEventHandler: function(selectorName, event) {
        var eventTarget = util.findEventTarget(event);
        var tooltipInstance = this.elementInstances[selectorName];
        var tooltipElement = eventTarget.querySelector('.tooltip');

        clearTimeout(tooltipInstance.timeout);

        tooltipInstance.timeout = null;
        util.removeClass(tooltipElement, 'show');

        tooltipInstance.removeToolTipElement(eventTarget);
    },
    _checkOverLapSelectorChk: function(selectorName) {
        var result = false;
        var regexp = RegExp('.*([.][-_a-zA-Z]*?)$');
        Object.keys(this.elementInstances).forEach(function(existSelectorName) {
            if (existSelectorName.replace(regexp, '$1') === selectorName.replace(regexp, '$1')) {
                result = true;
            }
        });

        return result;
    }
};

/**
 * remove all instance
 * @param {Object} elementInstances - tooltip map list
 */
function removeAllInstance(elementInstances) {
    tui.forEachOwnProperties(elementInstances, function(removeItem) {
        removeItem.removeTooltipEvent();
    });
}

/**
 * tooltip display timer
 * @param {Object} tooltipInstance - Tooltip Object
 * @param {HTMLElement} eventTarget - mouseleave event object
 */
function displayToolTip(tooltipInstance, eventTarget) {
    var tooltipElement = eventTarget.querySelector('.tooltip');
    tooltipInstance.timeout = setTimeout(function() {
        util.addClass(tooltipElement, 'show');
        tooltipInstance.timeout = null;
    }, tooltipInstance.delay);
}

/**
 * setting default option
 * @param {Object} userOption - default userOption
 * @returns {Object} option - remake option
 */
function setDefalutOption(userOption) {
    var option = {
        'delay': 0,
        'contents': 'empty'
    };
    if (userOption) {
        option = tui.extend(option, userOption);
    }

    return option;
}

module.exports = tooltip;

